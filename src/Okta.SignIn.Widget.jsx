import React, { Component } from 'react';
import { withOktaAuth } from '@okta/okta-react';
import OktaSignIn from '@okta/okta-signin-widget';
import '@okta/okta-signin-widget/dist/css/okta-sign-in.min.css';

class OktaSignInWidget extends Component {
  constructor(props) {
    super(props);
    this.wrapper = React.createRef();
  }
  componentDidMount() {
    this.widget = new OktaSignIn({
      baseUrl: this.props.baseUrl,
      logo: './acid_logo_black.png',
      logoText: 'Acid',
      language: 'es',
      i18n: {
        es: {
          'primaryauth.title': 'Iniciar Sesión en Acid Prueba'
        },
        en: {
          'primaryauth.title': 'Sign in to Acid Prueba'
        }
      }
    });
    this.widget.renderEl({el: this.wrapper.current}, this.props.onSuccess, this.props.onError);
  }

  componentWillUnmount() {
    this.widget.remove();
  }

  render() {
    return <div ref={this.wrapper} />;
  }
}

export default withOktaAuth(OktaSignInWidget);
