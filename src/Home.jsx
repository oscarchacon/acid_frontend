import { Link } from 'react-router-dom';

const Home = () => {
    return (
        <div className="page">
            <h1>Inicio de Sesión con React</h1>
            <Link to="/private">
                <button type="button">
                Visitar Página Restringida
                </button>
            </Link>
        </div>
    );
};

export default Home;