const Private = () => {
    return (
      <div className="page">
        <div className="warning">
          <h1>Área Restringida</h1>
          <h2>Solo Personas Autorizadas</h2>
        </div>
      </div>
    );
  }
  
  export default Private;