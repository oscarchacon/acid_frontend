import { Link, useHistory } from 'react-router-dom';
import { useOktaAuth } from '@okta/okta-react';

const Header = () => {
  const { oktaAuth, authState } = useOktaAuth();
  const history = useHistory();

  const login = () => { 
    //await oktaAuth.signInWithRedirect(); 
    history.push('/login');
  };
  const logout = async () => { await oktaAuth.signOut(); }

  const userText = authState.isAuthenticated
    ? <button onClick={ logout }>Cerrar Sesión</button>
    : <button onClick={ login }>Iniciar Sesión</button>;

  return (
    <header>
      <div>React Prueba Login</div>
      <ul className="menu">
        <li><Link to="/">Inicio</Link></li>
        <li><Link to="/private">Privada</Link></li>
      </ul>
      {userText}
    </header>
  );
}

export default Header;