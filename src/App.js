import './App.css';
import Header from './Header';
import Home from './Home';
import Private from './Private';
import Login from './Login';
import oktaAuth from './config/Okta.Auth';
import { LoginCallback, SecureRoute, Security } from '@okta/okta-react';
import { toRelativeUrl } from '@okta/okta-auth-js';
import { Route, useHistory } from 'react-router-dom';

function App() {
  const history = useHistory();
  const restoreOriginalUri = async (_oktaAuth, originalUri) => {
    history.replace(toRelativeUrl(originalUri, window.location.origin));
  };

  const onAuthRequired = () => {
    history.push('/login');
  };

  return (
    <div className="App">
      <div className="page">
        <div className="content">
          <Security 
            oktaAuth={oktaAuth} 
            restoreOriginalUri={restoreOriginalUri}
            onAuthRequired={onAuthRequired}
          >
            <Header/>
            <Route path='/' exact={true} component={Home}/>
            <Route path='/login' exact={true} component={Login}/>
            <SecureRoute path='/private' exact={true} component={Private}/>
            <Route path='/callback' component={LoginCallback}/>
          </Security>
        </div>
      </div>
    </div>
  );
}

export default App;
