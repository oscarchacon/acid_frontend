import { Redirect } from 'react-router-dom';
import { useOktaAuth } from '@okta/okta-react';
import OktaSignInWidget from './Okta.SignIn.Widget';

const Login = () => {
  const { oktaAuth, authState } = useOktaAuth();

  const onSuccess = (res) => {
    if (res.status === 'SUCCESS') {
      return oktaAuth.signInWithRedirect({
        sessionToken: res.session.token
      });
    }
  };

  const onError = (err) => {
    console.log('error logging in', err);
  };

  const baseUrlOkta = `https://${process.env.REACT_APP_OKTA_DOMAIN}`;

  return authState.isAuthenticated ?
    <Redirect to={{ pathname: '/' }}/> :
    <OktaSignInWidget
      baseUrl={baseUrlOkta}
      onSuccess={onSuccess}
      onError={onError}/>;
};

export default Login;